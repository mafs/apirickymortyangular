import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNoFound404Component } from './page-no-found404.component';

describe('PageNoFound404Component', () => {
  let component: PageNoFound404Component;
  let fixture: ComponentFixture<PageNoFound404Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageNoFound404Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNoFound404Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

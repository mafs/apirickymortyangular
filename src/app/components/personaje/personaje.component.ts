import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { IPersonaje } from 'src/app/interfaces';

@Component({
  selector: 'app-personaje',
  templateUrl: './personaje.component.html',
  styleUrls: ['./personaje.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonajeComponent implements OnInit {
  @Input() personajeInput: IPersonaje;
  @Output() eliminarPersonaje: EventEmitter<IPersonaje> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  eliminar(): void{
    this.eliminarPersonaje.emit(this.personajeInput);
  }

}

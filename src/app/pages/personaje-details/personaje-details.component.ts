import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Observable, throwError } from 'rxjs';
import { IPersonaje } from 'src/app/interfaces';
import { switchMap, catchError, retry } from 'rxjs/operators';
import { PersonajesService } from 'src/app/services/personajes.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-personaje-details',
  templateUrl: './personaje-details.component.html',
  styleUrls: ['./personaje-details.component.scss']
})
export class PersonajeDetailsComponent implements OnInit {
  personaje$: Observable<IPersonaje>;
  error: boolean = false;
  errorMessage: string;
  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private personajeSrv: PersonajesService
    ) { }

  ngOnInit(): void {
    this.personaje$ = this.route.params.pipe(
      retry(1),
      switchMap((params:Params) => {
        return this.personajeSrv.getCharacter(params['id'])
      }),
      catchError((err:HttpErrorResponse) => {
        this.errorMessage = err.message;
        this.error = true;
        return throwError(err);
      }),
    )
  }

  regresar(){
    this.location.back();
  }

}

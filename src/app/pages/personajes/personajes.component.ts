import { Component, OnInit, OnDestroy, Renderer2, HostListener, Inject, ChangeDetectionStrategy } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { PersonajesService } from '../../services/personajes.service';
import { IPersonaje } from 'src/app/interfaces';
import { Subscription, Observable, fromEvent } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { auditTime, switchMap, take } from 'rxjs/operators';


@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html',
  styleUrls: ['./personajes.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonajesComponent implements OnInit, OnDestroy {
  personajes: IPersonaje[] = [];
  // return an object
  // objRef = window.scrollbars
  currentScroll: number;
  name: String;
  termino: String;
  suscription: Subscription;
  dataFound: boolean = false;
  // mas:HTMLElement;
  haciendoPeticion: boolean = false;

  constructor(
    public personajesSrv: PersonajesService,
    private render: Renderer2,
    private title: Title,
    @Inject(DOCUMENT) private _document
    ) { }
  
  ngOnInit(): void {
    this._document.addEventListener('scroll', () => this.currentScroll = window.scrollY);
    this.obtener();
    this.personajesSrv.personaje$.subscribe(
      termino => {
        console.log('este es el termino', termino);
        this.personajesSrv.getTodosPersonajeByNAme(termino).subscribe(data => {
          this.personajes = data;
        })
      });
      // console.log('scroll', this.todo);
  }

  obtener(): void {
    this.suscription = this.personajesSrv.getTodosPersonajeByNAme()
    .subscribe(results => this.personajes = results);

  }

  usuarioEliminado(usuario: any){
    console.log(usuario);
  }
  

  obtenerPorNombre():void {
    this.personajesSrv.getTodosPersonajeByNAme(this.name).subscribe(data => {
      this.personajes = data;
    });
  }

  ngOnDestroy(): void {
    this.suscription.unsubscribe();
  }
  
  irArriba(){
    // recibe el scrollX y el scrollY
    window.scrollTo(0,0);
  }
  
  getEventScroll(): Observable<IPersonaje[]> {
    console.log('memooooooooooooo');
    console.log(this.currentScroll == (document.body.scrollHeight-1000), 'gggggg');
    if(this.currentScroll >= (document.body.scrollHeight-1000)) {
      return fromEvent(this._document, 'scroll').pipe(
        auditTime(1000),
        take(1),
        switchMap(() => this.personajesSrv.getNextCharacter())
      );
    }
  }

  doSomethingOnScroll(event){
    console.log(event, 'cache');
  }
  // @HostListener("window:scroll", ['$event'])
  // doSomethingOnInternalScroll($event:Event){
  //   console.log((document.body.scrollHeight), 'heigth');
  //   // console.log(this.currentScroll >= (document.body.scrollHeight-1000));
  //   if(this.currentScroll >= (document.body.scrollHeight-1000)) {
  //   //   console.log(this.currentScroll, (document.body.scrollHeight-1000));
  //   if(!this.dataFound) {
  //     this.dataFound = true;
  //     this.getEventScroll().subscribe(data => {
  //       this.personajes = [...this.personajes, ...data];
  //       console.log(this.personajes?.length);
  //       this.dataFound = false;
  //       setTimeout(() => window.scrollTo(0,(document.body.scrollHeight-1100)), 1500);
  //     });
  //   }
  //   }
  // }

  onScroll(){
    console.log('onScroll');
    // this.personajes = [...this.personajes, ...data]
    this.personajesSrv.getNextCharacter()
    .subscribe(caracteres => this.personajes = [...this.personajes, ...caracteres]);
  }

}

// if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';

import { rutasHijasModule } from './pages.routes';

import { InicioComponent } from './inicio/inicio.component';
import { PersonajesComponent } from './personajes/personajes.component';
import { PersonajeDetailsComponent } from './personaje-details/personaje-details.component';
import { PersonajeComponent } from '../components/personaje/personaje.component';
import { FilterPipe } from '../pipes/filter.pipe';
import { FormsModule } from '@angular/forms';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  declarations: [
    InicioComponent,
    PersonajesComponent,
    PersonajeDetailsComponent,
    PersonajeComponent,
    FilterPipe
  ],
  imports: [
    CommonModule,
    rutasHijasModule,
    SharedModule,
    FormsModule,
    InfiniteScrollModule
  ]
})
export class PagesModule { }

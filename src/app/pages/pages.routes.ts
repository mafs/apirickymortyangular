import { Routes, RouterModule } from '@angular/router';

import { InicioComponent } from './inicio/inicio.component';
import { PersonajesComponent } from './personajes/personajes.component';
import { PersonajeDetailsComponent } from './personaje-details/personaje-details.component';

const rutas: Routes = [
    { path: '',
    component: InicioComponent,
    children: [
        { path: '', component: PersonajesComponent, data: {titulo: 'personajes'} },
        { path: 'personaje/:id', component: PersonajeDetailsComponent, data:{titulo: 'persoanje detalle'} },
    ]},
];

export const rutasHijasModule = RouterModule.forRoot(rutas);
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { PagesModule } from './pages/pages.module';
import { SharedModule } from './shared/shared.module';

import { RutasPadreModule } from './app.routes';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PageNoFound404Component } from './page-no-found404/page-no-found404.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageNoFound404Component
  ],
  imports: [
    BrowserModule,
    PagesModule,
    RutasPadreModule,
    SharedModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

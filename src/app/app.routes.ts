import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PageNoFound404Component } from './page-no-found404/page-no-found404.component';

const rutas: Routes = [
    { path:'login', component: LoginComponent },
    { path: '', redirectTo: '/', pathMatch: 'full' },
    // { path: '**', component: PageNoFound404Component }
    { path: '**', pathMatch: 'full', redirectTo: '' }
];


export const RutasPadreModule = RouterModule.forRoot(rutas);